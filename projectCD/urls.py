"""projectCD URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import re_path
from appgege.views import TableTennis, StatusHTML, About, list_buku, table
from django.conf.urls import url, include
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from appgege.views import *
urlpatterns = [
    re_path(r'^admin/', admin.site.urls),
    re_path(r'^$', TableTennis, name='table'),
    re_path(r'^appgege/', TableTennis, name='table'),
    re_path(r'^status/', StatusHTML, name = 'StatusHTML'),
    re_path(r'^About/', About, name='About'),
    re_path(r'^list_buku/', list_buku, name='list_buku'),
    re_path(r'^table/', table, name='table2'),
    
]

urlpatterns += staticfiles_urlpatterns()
