from django.shortcuts import render
from .models import Table
from .forms import *
import json
import requests
from django.http import JsonResponse
# Create your views here.

def TableTennis(request):
    if (request.method == "POST"):
        form = FormTable(request.POST or None)
        if form.is_valid():
            box = request.POST["box"]
            post = Table(box=box)
            post.save()
    else:
        form = FormTable()
    all_table = Table.objects.all()
    return render(request, 'StatusHTML.html', {'Table': all_table,'form':form})

def StatusHTML(request):
    all_table = Table.objects.all()
    html = 'StatusHTML.html'
    return render(request, html, {'Table':all_table})
    
def About(request):
    html = "About.html"
    return render(request, html)

def table(request):
    html = "table.html"
    return render(request, html) 
    
def test(request):
    pass
    
def list_buku(request):
    return JsonResponse(requests.get('https://www.googleapis.com/books/v1/volumes?q=quilting').json())