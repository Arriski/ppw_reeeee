from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import TableTennis
from .models import Table
from .forms import FormTable
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

# Create your tests here.

class appgegeUnitTest(TestCase):
    
    def test_lab_6_url_is_exist(self):
        response = Client().get('/appgege/')
        self.assertEqual(response.status_code, 200)

    def test_lab5_using_index_func(self):
        found = resolve('/appgege/')
        self.assertEqual(found.func, TableTennis)
     
    def test_form_todo_input_has_placeholder_and_css_classes(self):
        response = Client().get("/appgege/")
        response_html = response.content.decode('utf8')
        self.assertContains(response, 'class="container"')

    def test_appgege_post_success_and_render_the_result(self):
        test = 'text'
        response_post = Client().post('/appgege', {'status': test})
        self.assertEqual(response_post.status_code, 301)
        response= Client().get('/appgege/')
        html_response = response.content.decode('utf8')
        self.assertIn(test, html_response)
        
    def test_appgege_post_error_and_render_the_result(self):
        test = 'Anonymous'
        response_post = Client().post('/appgege/', {'status': ''})
        self.assertEqual(response_post.status_code, 200)
        response= Client().get('/appgege/')
        html_response = response.content.decode('utf8')
        self.assertNotIn(test, html_response)
        
    def test_form_validation_for_blank_items(self):
        form = FormTable(data={'box': ''})
        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors['box'],["This field is required."])        

    def test_model_can_create_box(self):
        # Creating a new activity
        new_box = Table.objects.create(box='mengerjakan lab ppw')
        # Retrieving all available activity
        count_all_variable = Table.objects.all().count()
        self.assertEqual(count_all_variable, 1)

    def test_appgege_contains_hello(self):
        response = Client().get('/appgege/')
        response_html = response.content.decode('utf8')
        self.assertContains(response, 'Halo, apa kabar?')

class appgegeFunctionalTest(TestCase):
 
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
 

        self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(appgegeFunctionalTest, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(appgegeFunctionalTest, self).tearDown()

    def test_input_todo(self):
        selenium = self.selenium
        # Opening the link we want to test
        selenium.get('http://cdprojectrekt.herokuapp.com')
        # find the form element
        input = selenium.find_element_by_name('box')

        submit = selenium.find_element_by_id('submit')

        # Fill the form with data
        input.send_keys('PEPEWE REVOLUSI MENTAL 2018')

        # submitting the form
        submit.send_keys(Keys.RETURN)

        text = selenium.page_source
        self.assertIn('Arditio Triadi Riski', selenium.title)
        self.assertIn('PEPEWE REVOLUSI MENTAL 2018', text)
    def test_using_css_import_button(self):
        selenium = self.selenium
        selenium.get("http://cdprojectrekt.herokuapp.com")
        button = selenium.find_element_by_id("submit")
        self.assertEquals("bt1", button.get_attribute("class"))

    def test_align_value_form(self):
        selenium = self.selenium
        selenium.get("http://cdprojectrekt.herokuapp.com")
        form = selenium.find_element_by_name("box")
        self.assertEquals({'x':311,'y':178}, form.location)
        
    def test_align_value_button(self):
        selenium = self.selenium
        selenium.get("http://cdprojectrekt.herokuapp.com")
        button = selenium.find_element_by_id("submit")
        self.assertEquals({'x':48,'y':237}, button.location)